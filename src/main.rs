extern crate midgar;
extern crate cgmath;
extern crate rand;

mod app;
mod renderer;
mod world;
mod util;

fn main() {
	println!("Hello, world!");

	let app_config = midgar::MidgarAppConfig::new()
		.with_title("Pong")
		.with_screen_size((app::SCREEN_WIDTH as u32, app::SCREEN_HEIGHT as u32));

	let app: midgar::MidgarApp<app::GameApp> = midgar::MidgarApp::new(app_config);
	app.run();
}
