use midgar::{self, Midgar, KeyCode};
use world::GameWorld;
use renderer::GameRenderer;

pub const SCREEN_WIDTH: f32 = 640.0;
pub const SCREEN_HEIGHT: f32 = 400.0;

pub struct GameApp {
	world: GameWorld,
	renderer: GameRenderer,
}

impl midgar::App for GameApp {
	fn create(midgar: &Midgar) -> Self {

		GameApp {
			world: GameWorld::new(midgar),
			renderer: GameRenderer::new(midgar),
		}
	}

	fn step(&mut self, midgar: &mut Midgar) {
		let dt = midgar.time().delta_time() as f32;

		if midgar.input().was_key_pressed(KeyCode::Q) {
			midgar.set_should_exit();
			return;
		}

		self.world.update(midgar, dt);
		self.renderer.render(midgar, dt, &self.world);
	}
	// optional: resize, step. destroy
}
