#[derive(Clone, Copy, Debug)]
pub struct XY {
	pub x: f32,
	pub y: f32,
}

impl XY {
	pub fn new(x: f32, y: f32) -> Self {
		XY {
			x: x,
			y: y,
		}
	}
}

#[derive(Clone, Copy, Debug)]
pub struct Rectangle {
	pub top: f32,
	pub bottom: f32,
	pub left: f32,
	pub right: f32,
}

impl Rectangle {
	pub fn new(pos: &XY, dim: &XY) -> Self {
		Rectangle {
			top: pos.y,
			bottom: pos.y + dim.y,
			left: pos.x,
			right: pos.x + dim.x,
		}
	}
}