use midgar::{self, Midgar, Surface};
use midgar::graphics::sprite::{SpriteRenderer};
use midgar::graphics::shape::{ShapeRenderer};
use midgar::graphics::texture::TextureRegion;
use cgmath::{self};

use world::GameWorld;
use app;

const BACKGROUND_COLOR: [f32; 3] = [0.0; 3];

pub struct GameRenderer {
	sprite_renderer: SpriteRenderer,
	shape_renderer: ShapeRenderer,
}

impl GameRenderer {
	pub fn new(midgar: &Midgar) -> Self {
		let projection = cgmath::ortho(
			0.0,
			app::SCREEN_WIDTH,
			app::SCREEN_HEIGHT,
			0.0,
			-1.0,
			1.0
		);

		GameRenderer {
			sprite_renderer: SpriteRenderer::new(midgar.graphics().display(), projection),
			shape_renderer: ShapeRenderer::new(midgar.graphics().display(), projection),
		}
	}

	pub fn render(&mut self, midgar: &Midgar, dt: f32, world: &GameWorld) {
		let mut target = midgar.graphics().display().draw();
		target.clear_color(BACKGROUND_COLOR[0], BACKGROUND_COLOR[1], BACKGROUND_COLOR[2], 1.0);

		// Draw background
		self.shape_renderer.draw_filled_rect(0.0, 0.0, app::SCREEN_WIDTH, app::SCREEN_HEIGHT, BACKGROUND_COLOR, &mut target);
		
		let color = [0.55; 3];

		// Draw separator
		let offset = 10.0;
		let mut separator_height = app::SCREEN_HEIGHT - (offset * 2.0);
		if separator_height < 0.0 {
			separator_height = 0.0;
		}
		self.shape_renderer.draw_filled_rect(app::SCREEN_WIDTH / 2.0, offset, 2.0, separator_height, [0.6; 3], &mut target);

		// Draw ball
		let ball = world.ball();
		self.shape_renderer.draw_filled_rect(ball.pos().x, ball.pos().y, ball.dim().x, ball.dim().y, color, &mut target);

		// Draw paddles
		let paddle_left = world.paddle_left();
		self.shape_renderer.draw_filled_rect(paddle_left.pos().x, paddle_left.pos().y, paddle_left.dim().x, paddle_left.dim().y, color, &mut target);
		
		let paddle_right = world.paddle_right();
		self.shape_renderer.draw_filled_rect(paddle_right.pos().x, paddle_right.pos().y, paddle_right.dim().x, paddle_right.dim().y, color, &mut target);

		target.finish().unwrap();
	}
}