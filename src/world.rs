use midgar::{Midgar, KeyCode};
use rand::{thread_rng, Rng};

use app;
use util::{XY, Rectangle};

const PADDLE_OFFSET: f32 = 50.0;
const PADDLE_HEIGHT: f32 = 50.0;
const PADDLE_SPEED: f32 = 2.0;

const LINE_THICKNESS: f32 = 20.0;
const INITIAL_SPEED: f32 = 4.0;

pub struct GameWorld {
	ball: Ball,
	paddle_left: Paddle,
	paddle_right: Paddle,
}

impl GameWorld {
	pub fn new(midgar: &Midgar) -> Self {
		GameWorld {
			ball: Ball::new(),
			paddle_left: Paddle::new(PADDLE_OFFSET),
			paddle_right: Paddle::new(app::SCREEN_WIDTH - PADDLE_OFFSET),
		}
	}

	pub fn update(&mut self, midgar: &Midgar, dt: f32) {
		if midgar.input().is_key_held(KeyCode::Up) {
			let new_y = self.paddle_left.pos().y - PADDLE_SPEED;
			self.paddle_left.set_pos_y(new_y);
		} else if midgar.input().is_key_held(KeyCode::Down) {
			let new_y = self.paddle_left.pos().y + PADDLE_SPEED;
			self.paddle_left.set_pos_y(new_y);
		}

		self.computer_move_paddle();

		self.ball.step(dt);

		let ball_rect = Rectangle::new(self.ball.pos(), self.ball.dim());

		// Check if point scored
		if ball_rect.right > app::SCREEN_WIDTH {
			self.paddle_left.increment_score();
			println!("Player 1 score: {}", self.paddle_left.score());
			if self.paddle_left.score() > 2 {
				println!("Player 1 wins!");
				self.restart();
			} else {
				self.reset_ball(false);
			}
			return;
		} else if ball_rect.left < 0.0 {
			self.paddle_right.increment_score();
			println!("Player 2 score: {}", self.paddle_right.score());
			if self.paddle_right.score() > 2 {
				println!("Player 2 wins!");
				self.restart();
			} else {
				self.reset_ball(true);
			}
			return;
		}

		// Check for screen edge collision
		if ball_rect.top <= 0.0 || ball_rect.bottom >= app::SCREEN_HEIGHT {
			self.ball.flip_dir_y();
		}

		// Check for paddle collision
		let mut paddle_collided = false;
		if self.ball.dir().x < 0.0 {
			let paddle_left_rect = Rectangle::new(self.paddle_left.pos(), self.paddle_left.dim());
			paddle_collided = self.check_rect_collision(&ball_rect, &paddle_left_rect);

		} else if self.ball.dir().x > 0.0 {
			let paddle_right_rect = Rectangle::new(self.paddle_right.pos(), self.paddle_right.dim());
			paddle_collided = self.check_rect_collision(&ball_rect, &paddle_right_rect);
		}
		if paddle_collided {
			self.ball.flip_dir_x();
		}
	}

	fn check_rect_collision(&self, r1: &Rectangle, r2: &Rectangle) -> bool {
		return r1.left <= r2.right && r1.right >= r2.left && r1.top <= r2.bottom && r1.bottom >= r2.top;
	}

	fn restart(&mut self) {
		self.reset_ball(false);
		self.paddle_left.reset_score();
		self.paddle_right.reset_score();
	}

	fn reset_ball(&mut self, serve_right_paddle: bool) {
		self.ball = Ball::new();
		if serve_right_paddle {
			self.ball.flip_dir_x();
		}

		let mut rng = thread_rng();
		let roll: f32 = rng.gen();
		// Randomly throw ball up or down
		if roll < 0.5 {
			self.ball.flip_dir_y();
		}
	}

	fn computer_move_paddle(&mut self) {
		let epsilon = PADDLE_SPEED + 5.0;

		let paddle_mid = self.paddle_right.pos().y + (self.paddle_right.dim().y / 2.0);
		let ball_mid = self.ball.pos().y + (self.ball.dim().y / 2.0);
		let screen_mid = app::SCREEN_HEIGHT / 2.0;

		let pos_y = self.paddle_right.pos().y;

		// Center paddle if ball is moving away
		if self.ball.dir().x < 0.0 {
			if paddle_mid < screen_mid - epsilon{
				self.paddle_right.set_pos_y(pos_y + PADDLE_SPEED);
			} else if paddle_mid > screen_mid + epsilon{
				self.paddle_right.set_pos_y(pos_y - PADDLE_SPEED);
			}
		} else {
			let mut rng = thread_rng();
			let roll: f32 = rng.gen();

			// Make computer a bit more stupid
			// 90% chance that computer will keep moving in the same direction
			if roll < 0.9 {
				let dir_y = self.paddle_right.dir().y;
				self.paddle_right.set_pos_y(pos_y + (PADDLE_SPEED * dir_y));
				return;
			} 

			// Move paddle towards ball
			if paddle_mid < ball_mid {
				self.paddle_right.set_pos_y(pos_y + PADDLE_SPEED);
			} else {
				self.paddle_right.set_pos_y(pos_y - PADDLE_SPEED);
			}
		}
	}

	pub fn ball(&self) -> &Ball {
		&self.ball
	}

	pub fn paddle_left(&self) -> &Paddle {
		&self.paddle_left
	}

	pub fn paddle_right(&self) -> &Paddle {
		&self.paddle_right
	}
}

pub struct Ball {
	pos: XY,
	dir: XY, // -1.0 means left/up, 1.0 means right/down
	dim: XY,
	speed: XY,
}

impl Ball {
	pub fn new() -> Self {
		Ball {
			pos: XY::new(app::SCREEN_WIDTH / 2.0, app::SCREEN_HEIGHT / 2.0),
			dir: XY::new(-1.0, -1.0),
			dim: XY::new(LINE_THICKNESS, LINE_THICKNESS),
			speed: XY::new(INITIAL_SPEED, INITIAL_SPEED),
		}
	}
	pub fn step(&mut self, dt: f32) {
		let new_x = self.pos.x + self.dir.x * self.speed.x;
		let new_y = self.pos.y + self.dir.y * self.speed.y;
		self.pos = XY::new(new_x, new_y);
	}

	pub fn pos(&self) -> &XY {
		&self.pos
	}

	pub fn dir(&self) -> &XY {
		&self.dir
	}

	pub fn flip_dir_x(&mut self) {
		self.dir.x *= -1.0;
	}

	pub fn flip_dir_y(&mut self) {
		self.dir.y *= -1.0;
	}

	pub fn dim(&self) -> &XY {
		&self.dim
	}
}

pub struct Paddle {
	pos: XY,
	dir: XY,
	dim: XY,
	score: u32,
}

impl Paddle {
	pub fn new(x: f32) -> Self {
		Paddle {
			pos: XY::new(x, (app::SCREEN_HEIGHT / 2.0) - (PADDLE_HEIGHT / 2.0)),
			dim: XY::new(LINE_THICKNESS, PADDLE_HEIGHT),
			dir: XY::new(0.0, 0.0),
			score: 0,
		}
	}

	pub fn pos(&self) -> &XY {
		&self.pos
	}

	pub fn set_pos_y(&mut self, y: f32) {
		let mut new_y = y;
		if y < 0.0 {
			new_y = 0.0;
		} else if y + PADDLE_HEIGHT > app::SCREEN_HEIGHT {
			new_y = app::SCREEN_HEIGHT - PADDLE_HEIGHT;
		}

		if new_y > self.pos.y {
			self.dir.y = 1.0;
		} else {
			self.dir.y = -1.0;
		}

		self.pos.y = new_y;
	}

	pub fn dir(&self) -> &XY {
		&self.dir
	}

	pub fn flip_dir_y(&mut self) {
		self.dir.y *= -1.0;
	}

	pub fn dim(&self) -> &XY {
		&self.dim
	}

	pub fn score(&self) -> u32 {
		self.score
	}

	pub fn reset_score(&mut self) {
		self.score = 0;
	}

	pub fn increment_score(&mut self) {
		self.score += 1;
	}
}